"""
module for obtaining information about the recipients of EMAIL and SMS messages
from the collection. The collection settings are contained in the
configuration file "recipients_conf"
"""

import json
from urllib import quote

from recipients_conf import APP, COLLECTION
from collection.collection_rest import CollectionRest


def get_recipient(system, group):
    """
    extract information about recipients of EMAIL and SMS messages
    Args:
        system: str, system name (for example, "erib")
        group: str,  group name (for example, "admin")
    Raises:
        Exception: can not execute a rest request to Splunk server
    Returns:
        dict with phones and emails
        {}: recipient list is empty
    """
    try:

        search_query = json.dumps({
                'system':  (system),
                'group': {'$regex': group}
            })
        uri = '/servicesNS/nobody/%s/storage/collections/data/%s?query=%s' % (APP, COLLECTION, quote(search_query))
        col = CollectionRest()
        server_response = col.get_rest_data(uri)

    except Exception as ex:
        raise Exception('failed to get recipient: %s' % ex)

    if len(server_response) == 0:
        return {}

    recipient_list = []
    
    for item in server_response:    
        recipient_list.append(Recipient(item))

    return recipient_list


class Recipient:
    """docstring for Recipient"""
    def __init__(self, recipient):
        try:
            self.phone = recipient['phone']
            self.email = recipient['email']
            self.name = recipient['name']
            self.group = recipient['group']
            self.system = recipient['system']
            self.key = recipient.get('_key')
            self.recipient = recipient
        except KeyError as key_err:
            raise KeyError('field %s not set' % key_err)

