"""
module for setting up a collection that contains information about recipients of SMS and EMAIL messages
"""

# the name of the application where collection is located
APP = 'alert_manager'

# the name of the KVStore collection
COLLECTION = 'alert_mgr_distribution'

