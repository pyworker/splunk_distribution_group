# v.1.0
# coding: utf8
"""
Module for interaction with the KVStore using REST queries
"""

import json
import splunk.rest as rest
from utils import get_splunk_session_key


class CollectionRest:
    """
    Interaction with the server using the REST requests
    """
    def __init__(self):
        self._session_key = get_splunk_session_key()

    def get_rest_data(self, uri):
        """
        getting data from the server
        USAGE: get saved search or splunk configuration
        Args: 
            uri: str, URL address
        Raises: 
            Exception: can't connect to server
                     : can't parse response from server
            ValueError: invalid server responce
        Returns:
            response from server
        """
        response, content = rest.simpleRequest(uri, sessionKey=self._session_key, method='GET')

        if int(response.get('status')) != 200:
            raise Exception('failed to query the server: response code: %s' % response.get('status'))

        try:
            content = json.loads(content)
        except ValueError as val_err:
            raise ValueError('failed to query the server: invalid server responce: %s' % val_err)
        return content

    def post_rest_data(self, uri, data):
        """
        sending data to the server
        USAGE: update entry in KVStore or send data to KVStore
        Args:
            uri: str, URL address
            data: dict, data sending to KVStore

        Raises
            Exception: can't connect to server
        Returns: _id of added items
        """
        response, content = rest.simpleRequest(uri, sessionKey=self._session_key, jsonargs=data, method='POST')

        if int(response.get('status')) not in [200, 201]:
            raise Exception('failed to post data to server: response code %s' % response.get('status'))

        try:
            content = json.loads(content)
        except ValueError as val_err:
            raise ValueError('failed to query the server: invalid server responce: %s' % val_err)

        return content.get('_key')

    def delete_rest_data(self, uri):
        """
        deleting data from the KVStore
        Args:
            uri:  URL address
        :return:
        """

        # TODO: fix delete method. whats server status after delete?

        response, content = rest.simpleRequest(uri, sessionKey=self._session_key, method='DELETE')


