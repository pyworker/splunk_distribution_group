# v.1.0
from collection_rest import CollectionRest
from collection_api import CollectionAPI

import json
import unittest
import urllib
from datetime import datetime
import time

COLLECTION_NAME = 'test_collection'


class TestCollectionAPI(unittest.TestCase):

    def test_correct_work_create_get_delete(self):
        """
        testing the correct create, get and delete operation
        Raises: None
        """
        help = CollectionAPI()
        col = CollectionRest()

        try:
            help.get(collection_name=COLLECTION_NAME)
            help.delete()
        except Exception:
            # ignore
            pass

        help.create(collection_name=COLLECTION_NAME)

        timestamp = int(time.mktime(datetime.now().timetuple()))

        # try to post entry
        entry = json.dumps({'message': 'info', 'val': 1, 'timestamp': timestamp})
        uri = '/servicesNS/nobody/alert_manager/storage/collections/data/%s' % COLLECTION_NAME

        key = col.post_rest_data(uri, data=entry)
        should_answer = json.dumps([{
            '_key': str(key),
            '_user': 'nobody',
            'message': 'info',
            'timestamp': timestamp,
            'val': 1
        }])

        query = json.dumps({
            'message': 'info',
            'val': 1,
            'timestamp': timestamp
        })

        uri = (
            '/servicesNS/nobody/alert_manager/storage/collections/data/%s?query=%s'
            % (COLLECTION_NAME, urllib.quote(query))
        )

        answer = json.dumps(col.get_rest_data(uri=uri))
        self.assertEqual(answer, should_answer)

        col.delete_rest_data(uri=uri)
        help.delete()


if __name__ == '__main__':
    unittest.main()

