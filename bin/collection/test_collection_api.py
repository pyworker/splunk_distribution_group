# v.1.3
# coding: utf8

import unittest
import time
import json
from datetime import datetime

from collection_api import CollectionAPI

"""
example start test
run test ONLY under the folder where test script sets
C:\\project\\ot\\sync_splunk_user>"C:\\Program Files\\Splunk\\bin\\splunk.exe" cmd python -m unittest test_collection
"""

COLLECTION_NAME = 'test_collection'

# col = None
#
# def setUpModule():
#     col = CollectionAPI()
#
# def tearDownModule():
#     pass
#     # closeConnection()


class TestCollectionAPI(unittest.TestCase):

    def test_correct_work_create_get_delete(self):
        """
        testing the correct create, get and delete operation
        Raises: None
        """
        col = CollectionAPI()

        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass

        should_answer = 0
        col.create(COLLECTION_NAME)
        answer = col.get(COLLECTION_NAME)
        self.assertEqual(answer, should_answer)
        col.delete()

    def test_create_col_already_exist(self):
        """
        testing an attempt to create already existing collection
        Raises: Exception
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass

        col.create('test_collection')
        with self.assertRaises(Exception) as context:
            col.create('test_collection')

        self.assertTrue(
            (
                'error with create collection "%s": '
                '"HTTP 409 Conflict -- An object with name=test_collection already exists"' % COLLECTION_NAME
            )
            in context.exception
        )

    def test_delete_col_not_exist(self):
        """
        testing an attempt to delete not existing collection
        Raises: None
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass

        # double deleted is normal
        col.create(collection_name=COLLECTION_NAME)
        col.delete()
        answer = col.delete()
        should_answer = 0
        self.assertEqual(answer, should_answer)

    def test_delete_col_not_define(self):
        """
        test delete method
        Raises: KeyError
        """
        col = CollectionAPI()
        with self.assertRaises(KeyError) as context:
            col.delete()
        self.assertTrue('collection not defined' in context.exception)

    def test_get_col_not_exist(self):
        """
        testing an attempt to use an not existing collection
        Raises: KeyError
        """
        col = CollectionAPI()

        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass

        with self.assertRaises(KeyError) as context:
            col.get(COLLECTION_NAME)
        self.assertTrue(('collection "%s" not defined. use "create()" method' % COLLECTION_NAME) in context.exception)

    def test_post_get_delete_entry(self):
        """
        - testing the correct post get and delete operations
        - testing type query dict and str
        - testing delete by id
        Raises: None
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass

        col.create(collection_name=COLLECTION_NAME)
        timestamp = int(time.mktime(datetime.now().timetuple()))

        # try to post dictionary
        entry = {'message': 'info', 'val': 1, 'timestamp': timestamp}

        key = col.post_entry(entry=entry)
        should_answer = json.dumps([{
            '_key': str(key),
            '_user': 'nobody',
            'message': 'info',
            'timestamp': timestamp,
            'val': 1
        }])

        query = json.dumps({
            'message': 'info',
            'val': 1,
            'timestamp': timestamp
        })

        answer = json.dumps(col.get_entry(query=query))
        # self.assertEqual(answer, should_answer)
        self.assertTrue(should_answer in answer)

        # try to post string
        entry = json.dumps(entry)
        key = col.post_entry(entry=entry)

        # try to get with dict query
        col.get_entry(query={'_key': key})

        # try to delete by key
        col.delete_entry(query={'_key': key})

        col.delete_entry(query=query)
        col.delete()

    def test_post_existing_key(self):
        """
        testing an attempt to post entry with existing key
        Raises:
        """
        pass

    def test_post_col_not_define(self):
        """
        testing an attempt to poet entry to not define collection
        Raises: KeyError
        """
        col = CollectionAPI()
        entry = json.dumps({'message': 'info'})
        with self.assertRaises(KeyError) as context:
            col.post_entry(entry=entry)
        self.assertTrue('collection not defined. use "create()" or "get(collection_name)" methods' in context.exception)

    def test_post_invalid_type_query(self):
        """
        test an attempt to use no str or dict type of query
        Raises: KeyError
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass
        col.create(collection_name=COLLECTION_NAME)
        entry = 1
        with self.assertRaises(KeyError) as context:
            col.post_entry(entry=entry)

        self.assertTrue('invalid type of param: "1" expected "String" or "Dictionary"' in context.exception)

        entry = [1, 2]
        with self.assertRaises(KeyError) as context:
            col.post_entry(entry=entry)
        self.assertTrue('invalid type of param: "[1, 2]" expected "String" or "Dictionary"' in context.exception)

    def test_get_invalid_type_query(self):
        """
        test an attempt to use no str or dict type of query
        Raises: KeyError
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass
        col.create(collection_name=COLLECTION_NAME)
        query = 1
        with self.assertRaises(KeyError) as context:
            col.get_entry(query=query)
        self.assertTrue('invalid type of query: "1" expected "String" or "Dictionary"' in context.exception)

        query = [1, 2]
        with self.assertRaises(KeyError) as context:
            col.get_entry(query=query)
        self.assertTrue('invalid type of query: "[1, 2]" expected "String" or "Dictionary"' in context.exception)

    def test_delete_entry_invalid_type_query(self):
        """
        test an attempt to use no str or dict type of query
        Raises: KeyError
        """
        col = CollectionAPI()
        try:
            col.get(collection_name=COLLECTION_NAME)
            col.delete()
        except Exception:
            # ignore
            pass
        col.create(collection_name=COLLECTION_NAME)
        query = 1
        with self.assertRaises(KeyError) as context:
            col.delete_entry(query=query)
        self.assertTrue('invalid type of query: "1" expected "String" or "Dictionary"' in context.exception)

        query = [1, 2]
        with self.assertRaises(KeyError) as context:
            col.delete_entry(query=query)
        self.assertTrue('invalid type of query: "[1, 2]" expected "String" or "Dictionary"' in context.exception)

    def test_get_entry_col_not_define(self):
        """
        testing an attempt to get entry to not define collection
        Raises: KeyError
        """
        col = CollectionAPI()
        query = {'massage': 'info', 'val': 1}
        with self.assertRaises(KeyError) as context:
            col.get_entry(query=query)
        self.assertTrue('collection not defined. use "create()" or "get(collection_name)" methods' in context.exception)

    def test_delete_entry_col_not_define(self):
        """
        testing an attempt to get entry to not define collection
        Raises: KeyError
        """
        col = CollectionAPI()
        query = {'massage': 'info', 'val': 1}
        with self.assertRaises(KeyError) as context:
            col.delete_entry(query=query)
        self.assertTrue('collection not defined. use "create()" or "get(collection_name)" methods' in context.exception)


if __name__ == '__main__':
    unittest.main()
