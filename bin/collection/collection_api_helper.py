# v.1.3
# coding: utf8
"""
Module for realise helper method
"""

import time
import json
from datetime import datetime
from collection_api import CollectionAPI

from collection_api_conf import time_field


class APIHelper:
    def __init__(self, collection_name):
        self._col = CollectionAPI()
        self._col.get(collection_name)

    def get_all_collection_items(self):
        """
        метод вывода списка элементов коллекции
        :return: элементы коллекции
        :rtype: `List`
        """
        return self._col.get_entry(query={})

    def delete_all_items(self):
        """
        method for deleting all items from KVStore collection
        :return:
        """
        try:
            self._col.delete_entry(query={})
        except Exception as ex:
            print 'catch exception %s' % ex

    def generate_items(self, count, time_format='iso'):
        """
        method of generating entries to the KVStore collection
        Args:
            count: int, the number of entries generated in the KVStore collection
            time_format: str, the format used for the timestamp of each element of the collection
        Raises:
            Excepted: invalid timestamp format
        Returns:
            keys added to KVStore collections
        Rtype: List
        """

        allow_time_format = ['iso', 'epoch']
        if time_format not in allow_time_format:
            raise Exception('error time format %s expected "iso" or "epoch"' % time_format)

        if time_format == 'iso':
            timestamp = datetime.now().isoformat()
        else:
            timestamp = time.mktime(datetime.now().timetuple())

        res_list = []
        for cc in range(0, count):
            entry = json.dumps({'test_key': cc, 'val': (100 + cc), time_field: timestamp})
            res = self._col.post_entry(entry)
            res_list.append(res)
        return res_list
