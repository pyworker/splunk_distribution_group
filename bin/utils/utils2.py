# v.1.0
# coding: utf-8
"""
module with auxiliary functions using Splunk-SDK
"""

import splunk
from otcommon.wing import Wing


def get_splunk_session_key():
    """
    get new Splunk session key
    Args:
    Raises:
        Exception cant get info from splunk
    Returns:
        new Splunk session key
    """
    try:
        wing = Wing()
        session_key = wing.read()
        # initialization of the session key in the Splunk
        splunk.setDefault('sessionKey', session_key)
    except Exception as ex:
        raise Exception('failed to get Splunk session_key % s' % ex)

    return session_key


